-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:8889
-- Généré le :  Ven 13 Octobre 2017 à 16:19
-- Version du serveur :  5.6.35
-- Version de PHP :  7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `location`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cin` varchar(255) NOT NULL,
  `salaire` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `nom`, `prenom`, `tel`, `email`, `cin`, `salaire`) VALUES
(1, 'commercial', 'pass', 'KAMAL', 'kamal', '0634567894', 'kamal@gmail.com', 'IB215946', 8000);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cin` varchar(255) NOT NULL,
  `numeroPermis` varchar(255) NOT NULL,
  `numeroCompte` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `prenom`, `tel`, `email`, `cin`, `numeroPermis`, `numeroCompte`, `username`, `password`) VALUES
(1, 'Karimi1', 'mohammed', '0623456789', 'karimi@gmail.com', 'IB215467', '911091204209', '3242-5224-2424-12323', 'karimll', 'pass'),
(2, 'Joud', 'Laila', '0654637281', 'joud@gmail.com', 'ib345271', '9110912042034', '3242-5224-2424-8764', 'joudL', 'pass');

-- --------------------------------------------------------

--
-- Structure de la table `model`
--

CREATE TABLE `model` (
  `id` int(11) NOT NULL,
  `marque` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `prixLocation` float NOT NULL,
  `nombreVoiture` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `model`
--

INSERT INTO `model` (`id`, `marque`, `type`, `prixLocation`, `nombreVoiture`) VALUES
(1, 'Mercedes', 'benz AMG', 500, 5),
(2, 'Audi', 'A7', 500, 6);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `dateReservation` varchar(255) NOT NULL,
  `dateDebutLocation` varchar(255) NOT NULL,
  `dateFinLocation` varchar(255) NOT NULL,
  `isValide` int(11) NOT NULL DEFAULT '0',
  `id_client` int(11) NOT NULL,
  `id_voiture` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `reservation`
--

INSERT INTO `reservation` (`id`, `dateReservation`, `dateDebutLocation`, `dateFinLocation`, `isValide`, `id_client`, `id_voiture`) VALUES
(1, '13-10-2017 09:00', '01-11-2017 10:00', '10-11-2017 18:00', 0, 1, 4),
(2, '14-10-2017 10:00', '03-11-2017 11:00', '09-11-2017 10:00', 0, 2, 5),
(3, '03-10-2017 14:00', '05-11-2017 14:00', '08-11-2017 14:00', 0, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `voiture`
--

CREATE TABLE `voiture` (
  `id` int(11) NOT NULL,
  `dateMiseEnService` varchar(255) NOT NULL,
  `numerotation` varchar(255) NOT NULL,
  `id_model` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `voiture`
--

INSERT INTO `voiture` (`id`, `dateMiseEnService`, `numerotation`, `id_model`) VALUES
(1, '01-04-2017 00:00', '13563-B', 1),
(2, '11-04-2017 00:00', '13563-C', 1),
(3, '10-02-2017 08:00', '13543-A', 1),
(4, '14-05-2017 08:00', '13563-A', 2),
(5, '14-07-2017 08:00', '23563-B', 2),
(6, '12-12-2017 08:00', '64563-M', 2),
(7, '17-11-2017 08:00', '753563-K', 2);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Index pour la table `model`
--
ALTER TABLE `model`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD KEY `id_voiture` (`id_voiture`),
  ADD KEY `id_client` (`id_client`);

--
-- Index pour la table `voiture`
--
ALTER TABLE `voiture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_model` (`id_model`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `model`
--
ALTER TABLE `model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `voiture`
--
ALTER TABLE `voiture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`id_voiture`) REFERENCES `voiture` (`id`);

--
-- Contraintes pour la table `voiture`
--
ALTER TABLE `voiture`
  ADD CONSTRAINT `voiture_ibfk_1` FOREIGN KEY (`id_model`) REFERENCES `model` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
